package com.example.marlon.kitsuclientchallenge.utils

import android.content.Context
import android.content.Intent
import android.net.Uri

// Class to open YouTube App
class YouTubeApp {
    companion object {
        // Open a video in Youtube App
        fun watchInYoutube(context: Context, videoId: String) {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:$videoId"))
            intent.putExtra("VIDEO_ID", videoId)
            context.startActivity(intent)
        }
    }
}