package com.example.marlon.kitsuclientchallenge.network


import com.example.marlon.kitsuclientchallenge.model.AnimeData
import com.example.marlon.kitsuclientchallenge.model.GenreData
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    // Search animes by text
    @GET("edge/anime")
    fun searchAnimeByText(
        @Query("page[limit]") pageLimit: String = "20",
        @Query("page[offset]") initialPage: String = "0",
        @Query("filter[text]") query: String
    ): Call<AnimeData?>

    // Search animes by genre
    @GET("edge/anime")
    fun searchAnimeByGenre(
        @Query("page[limit]") pageLimit: String = "20",
        @Query("page[offset]") initialPage: String = "0",
        @Query("filter[genres]") genre: String
    ): Call<AnimeData?>

    // Get the genres
    @GET("edge/genres")
    fun getGenres(
        @Query("page[limit]") pageLimit: String = "15",
        @Query("page[offset]") initialPage: String = "0"
    ): Call<GenreData?>

    // Get the anime's genres
    @GET("edge/anime/{animeId}/genres")
    fun getAnimeGenres(@Path("animeId") id: String?): Call<GenreData?>
}