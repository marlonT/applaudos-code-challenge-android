package com.example.marlon.kitsuclientchallenge.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import com.example.marlon.kitsuclientchallenge.R
import com.example.marlon.kitsuclientchallenge.adapter.AnimeDetailAdapter
import com.example.marlon.kitsuclientchallenge.model.Anime
import com.example.marlon.kitsuclientchallenge.model.KitsuDatabase
import com.example.marlon.kitsuclientchallenge.repository.AnimeRepository
import com.example.marlon.kitsuclientchallenge.utils.YouTubeApp
import kotlinx.android.synthetic.main.fragment_anime_detail.view.*

class AnimeDetailFragment : Fragment(), AnimeDetailAdapter.SelectedOption,
    AnimeRepository.AnimeResponse {
    companion object {
        const val POSITION = "position key"
        const val GENRE = "genre key"
    }

    private lateinit var adapter: AnimeDetailAdapter
    private lateinit var viewManager: LinearLayoutManager
    private var genre: String? = null
    private var position: Int = 0

    private var database: KitsuDatabase? = null
    private var animeRepository: AnimeRepository? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            genre = it.getString(GENRE)
            position = it.getInt(POSITION)

            database = KitsuDatabase.getDatabase(context!!)
            animeRepository = AnimeRepository(database?.animeDao(), this)
            animeRepository?.searchAnimeByGenre(genre!!)
        }
    }

    // Open a youtube video in YouTube app
    override fun openYouTube(id: String) {
        YouTubeApp.watchInYoutube(context!!, id)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_anime_detail, container, false)
        val recyclerView = view.anime_list

        viewManager = LinearLayoutManager(
            context,
            LinearLayoutManager.HORIZONTAL,
            false
        )

        adapter = AnimeDetailAdapter(context!!, this)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = viewManager
        // It allows the Navigation view as pages, through the recycler view
        val snapHelper = PagerSnapHelper()
        snapHelper.attachToRecyclerView(recyclerView)

        return view
    }

    override fun onAnimesResult(animes: ArrayList<Anime?>, genre: String) {
        adapter.setAnimes(animes)
        viewManager.scrollToPosition(position)
    }

}