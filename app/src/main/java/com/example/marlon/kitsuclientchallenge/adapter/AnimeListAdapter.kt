package com.example.marlon.kitsuclientchallenge.adapter

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.example.marlon.kitsuclientchallenge.R
import com.example.marlon.kitsuclientchallenge.model.Anime
import com.example.marlon.kitsuclientchallenge.utils.GlideApp
import kotlinx.android.synthetic.main.anime_item.view.*

class AnimeListAdapter(
    context: Context,
    private val selectedItem: SelectedItem
) : RecyclerView.Adapter<AnimeListAdapter.AnimeViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var animes: ArrayList<Anime?> = ArrayList()
    private var genre: String? = null

    inner class AnimeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    interface SelectedItem {
        fun onItemSelected(position: Int, genre: String)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AnimeViewHolder {
        val itemView = inflater.inflate(R.layout.anime_item, parent, false)
        return AnimeViewHolder(itemView)
    }

    // Show the anime's name and image
    override fun onBindViewHolder(holder: AnimeViewHolder, position: Int) {
        val current = animes[position]
        holder.itemView.setOnClickListener { selectedItem.onItemSelected(position, genre!!) }
        holder.itemView.animeName.text = current?.attributes?.canonicalTitle
        val imgUrl = current?.attributes?.posterImage?.original
        val thumbnail = holder.itemView.animeThumbnail
        val progress = holder.itemView.progressThumbnail
        GlideApp.with(holder.itemView)
            .load(imgUrl)
            .override(350, 500)
            .centerCrop()
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    progress.visibility = View.GONE
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    progress.visibility = View.GONE
                    return false
                }
            })
            .into(thumbnail)
    }

    internal fun setAnimes(animes: ArrayList<Anime?>, genre: String) {
        this.animes = animes
        this.genre = genre
        notifyDataSetChanged()
    }

    override fun getItemCount() = animes.size
}