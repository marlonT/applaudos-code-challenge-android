package com.example.marlon.kitsuclientchallenge.model

import com.google.gson.annotations.SerializedName

class AnimeData(
    @SerializedName("data")
    val data: ArrayList<Anime?>
)

