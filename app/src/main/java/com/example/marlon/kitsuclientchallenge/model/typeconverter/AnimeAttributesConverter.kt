package com.example.marlon.kitsuclientchallenge.model.typeconverter

import androidx.room.TypeConverter
import com.example.marlon.kitsuclientchallenge.model.AnimeAttributes
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type


class AnimeAttributesConverter {
    @TypeConverter
    fun fromAnimeAttributes(animeAttributes: AnimeAttributes?): String? {
        val gson = Gson()
        val type = object : TypeToken<AnimeAttributes?>() {}.type
        return gson.toJson(animeAttributes, type)
    }

    @TypeConverter
    fun toAnimeAttributes(animeAttributesString: String?): AnimeAttributes? {
        val gson = Gson()
        val type: Type = object : TypeToken<AnimeAttributes?>() {}.type
        return gson.fromJson(animeAttributesString, type)
    }
}