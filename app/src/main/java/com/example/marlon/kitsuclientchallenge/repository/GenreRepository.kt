package com.example.marlon.kitsuclientchallenge.repository

import com.example.marlon.kitsuclientchallenge.dao.GenreDao
import com.example.marlon.kitsuclientchallenge.model.Genre
import com.example.marlon.kitsuclientchallenge.model.GenreData
import com.example.marlon.kitsuclientchallenge.network.Api
import com.example.marlon.kitsuclientchallenge.network.ApiService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GenreRepository(
    val genreDao: GenreDao?,
    val genreResponse: GenreResponse
) {
    private val client = Api.getApi()?.create(ApiService::class.java)

    interface GenreResponse {
        fun onGenresResult(genres: ArrayList<Genre?>)
    }

    fun getGenres() {
        client?.getGenres()?.enqueue(object : Callback<GenreData?> {
            override fun onFailure(call: Call<GenreData?>, t: Throwable) {
                genreResponse.onGenresResult(runBlocking(Dispatchers.IO) { ArrayList(genreDao!!.getAllGenres()) })
            }

            override fun onResponse(
                call: Call<GenreData?>,
                response: Response<GenreData?>
            ) {
                genreResponse.onGenresResult(genres = response.body()!!.data)
            }
        })
    }
}