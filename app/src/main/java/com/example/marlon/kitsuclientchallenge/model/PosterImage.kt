package com.example.marlon.kitsuclientchallenge.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class PosterImage(
    @SerializedName("medium")
    val original: String?
) : Parcelable {
    constructor(parcel: Parcel) : this(parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(original)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PosterImage> {
        override fun createFromParcel(parcel: Parcel): PosterImage {
            return PosterImage(parcel)
        }

        override fun newArray(size: Int): Array<PosterImage?> {
            return arrayOfNulls(size)
        }
    }
}
