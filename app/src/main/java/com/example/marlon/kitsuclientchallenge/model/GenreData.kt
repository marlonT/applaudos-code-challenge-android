package com.example.marlon.kitsuclientchallenge.model

import com.google.gson.annotations.SerializedName

class GenreData(
    @SerializedName("data")
    val data: ArrayList<Genre?>
)