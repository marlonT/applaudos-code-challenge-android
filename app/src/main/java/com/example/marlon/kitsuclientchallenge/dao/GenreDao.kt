package com.example.marlon.kitsuclientchallenge.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.marlon.kitsuclientchallenge.model.Genre

@Dao
interface GenreDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertGenres(genres: List<Genre?>)

    @Query("SELECT DISTINCT * from Genre")
    fun getAllGenres(): List<Genre?>

    @Query("DELETE from Genre")
    fun clearAllGenres()
}