package com.example.marlon.kitsuclientchallenge.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class AnimeAttributes(
    @SerializedName("slug")
    val mainTitle: String?,
    @SerializedName("canonicalTitle")
    val canonicalTitle: String?,
    @SerializedName("subtype")
    val showType: String?,
    @SerializedName("episodeCount")
    val episodeCount: String?,
    @SerializedName("startDate")
    val startDate: String?,
    @SerializedName("endDate")
    val endDate: String?,
    @SerializedName("averageRating")
    val averageRating: String?,
    @SerializedName("ageRating")
    val ageRating: String?,
    @SerializedName("episodeLength")
    val episodeLength: String?,
    @SerializedName("status")
    val airingStatus: String?,
    @SerializedName("synopsis")
    val synopsis: String?,
    @SerializedName("youtubeVideoId")
    val youtubeVideoId: String?,
    @SerializedName("posterImage")
    val posterImage: PosterImage?,

    var genres: ArrayList<Genre?>? = null
) : Parcelable {

    fun genresToString(): String {
        var genresString = ""
        genres?.forEach {
            genresString += if (genres?.indexOf(it) == 0) {
                it?.attributes?.name
            } else {
                ", " + it?.attributes?.name
            }
        }
        return genresString
    }

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readParcelable(PosterImage::class.java.classLoader)
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(mainTitle)
        parcel.writeString(canonicalTitle)
        parcel.writeString(showType)
        parcel.writeString(episodeCount)
        parcel.writeString(startDate)
        parcel.writeString(endDate)
        parcel.writeString(averageRating)
        parcel.writeString(ageRating)
        parcel.writeString(episodeLength)
        parcel.writeString(airingStatus)
        parcel.writeString(synopsis)
        parcel.writeString(youtubeVideoId)
        parcel.writeParcelable(posterImage, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AnimeAttributes> {
        override fun createFromParcel(parcel: Parcel): AnimeAttributes {
            return AnimeAttributes(parcel)
        }

        override fun newArray(size: Int): Array<AnimeAttributes?> {
            return arrayOfNulls(size)
        }
    }
}
