package com.example.marlon.kitsuclientchallenge.repository

import com.example.marlon.kitsuclientchallenge.dao.AnimeDao
import com.example.marlon.kitsuclientchallenge.model.Anime
import com.example.marlon.kitsuclientchallenge.model.AnimeData
import com.example.marlon.kitsuclientchallenge.model.GenreData
import com.example.marlon.kitsuclientchallenge.network.Api
import com.example.marlon.kitsuclientchallenge.network.ApiService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AnimeRepository(val animeDao: AnimeDao?,val animeResponse: AnimeResponse) {
    val client = Api.getApi()?.create(ApiService::class.java)

    interface AnimeResponse {
        fun onAnimesResult(animes: ArrayList<Anime?>, genre: String)
    }

    fun searchAnimeByGenre(genre: String) {

        var animes: ArrayList<Anime?>?
        client?.searchAnimeByGenre(genre=genre)?.enqueue(object : Callback<AnimeData?> {
            override fun onFailure(call: Call<AnimeData?>, t: Throwable) {
                animes = ArrayList(animeDao!!.getAllAnimes()!!.toMutableList())
                animeResponse.onAnimesResult(animes!!,genre)
            }
            override fun onResponse(
                call: Call<AnimeData?>,
                response: Response<AnimeData?>
            ) {
                animes=response.body()?.data
                animes?.forEach {
                    client.getAnimeGenres(it?.id).enqueue(object :Callback<GenreData?>{
                        override fun onFailure(call: Call<GenreData?>, t: Throwable) {
                            it?.attributes?.genres=null
                            animeResponse.onAnimesResult(animes!!,genre)
                        }

                        override fun onResponse(
                            call: Call<GenreData?>,
                            response: Response<GenreData?>
                        ) {
                            it?.attributes?.genres=response.body()?.data
                            if (animes?.indexOf(it)==(animes?.size?.minus(1))){
                                animeResponse.onAnimesResult(animes!!,genre)
                            }
                        }

                    })
                }
            }
        })
    }

}