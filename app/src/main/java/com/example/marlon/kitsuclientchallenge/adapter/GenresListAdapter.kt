package com.example.marlon.kitsuclientchallenge.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.marlon.kitsuclientchallenge.R
import com.example.marlon.kitsuclientchallenge.model.Anime
import com.example.marlon.kitsuclientchallenge.model.Genre
import com.example.marlon.kitsuclientchallenge.model.KitsuDatabase
import com.example.marlon.kitsuclientchallenge.repository.AnimeRepository
import kotlinx.android.synthetic.main.genre_item.view.*

class GenresListAdapter(
    private val context: Context,
    private val selectedItem: AnimeListAdapter.SelectedItem
) : RecyclerView.Adapter<GenresListAdapter.GenreViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var genres: ArrayList<Genre?> = arrayListOf()
    private var database: KitsuDatabase? = null
    private var animeRepository: AnimeRepository? = null

    inner class GenreViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        AnimeRepository.AnimeResponse {

        lateinit var adapter: AnimeListAdapter
        override fun onAnimesResult(animes: ArrayList<Anime?>, genre: String) {
            adapter.setAnimes(animes, genre)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenreViewHolder {
        val itemView = inflater.inflate(R.layout.genre_item, parent, false)
        return GenreViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: GenreViewHolder, position: Int) {
        database = KitsuDatabase.getDatabase(context)
        animeRepository = AnimeRepository(database?.animeDao(), holder)
        val genre = genres[position]?.attributes?.name
        animeRepository?.searchAnimeByGenre(genre = genre!!)

        holder.itemView.genre.text = genre
        // Show the anime's list by genre
        val recyclerView = holder.itemView.animeRecycler
        holder.adapter = AnimeListAdapter(context, selectedItem)
        recyclerView.adapter = holder.adapter
        recyclerView.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
    }

    internal fun setGenres(genres: ArrayList<Genre?>) {
        this.genres = genres
        notifyDataSetChanged()
    }

    override fun getItemCount() = genres.size
}