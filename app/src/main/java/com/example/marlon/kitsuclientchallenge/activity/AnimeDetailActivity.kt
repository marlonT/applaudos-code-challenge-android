package com.example.marlon.kitsuclientchallenge.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.marlon.kitsuclientchallenge.R
import com.example.marlon.kitsuclientchallenge.fragment.AnimeDetailFragment

class AnimeDetailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // Creates a new fragment
        val animeListFragment = AnimeDetailFragment()
        // Send data to the fragment
        animeListFragment.arguments = intent.extras
        supportFragmentManager.beginTransaction().replace(R.id.container_layout, animeListFragment)
            .commit()
    }
}