package com.example.marlon.kitsuclientchallenge.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.marlon.kitsuclientchallenge.model.Anime

@Dao
interface AnimeDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAnimes(animes: List<Anime?>)

    @Query("SELECT * from Anime")
    fun getAllAnimes(): List<Anime?>?

    @Query("DELETE from Anime")
    fun clearAllAnimes()
}