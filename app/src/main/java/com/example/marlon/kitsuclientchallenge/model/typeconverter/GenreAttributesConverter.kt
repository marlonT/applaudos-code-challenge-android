package com.example.marlon.kitsuclientchallenge.model.typeconverter

import androidx.room.TypeConverter
import com.example.marlon.kitsuclientchallenge.model.GenreAttributes
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

class GenreAttributesConverter {

    @TypeConverter
    fun fromGenreAttributes(genreAttributes: GenreAttributes?): String? {
        val gson = Gson()
        val type = object : TypeToken<GenreAttributes?>() {}.type
        return gson.toJson(genreAttributes, type)
    }

    @TypeConverter
    fun toGenreAttributes(genreAttributesString: String?): GenreAttributes? {
        val gson = Gson()
        val type: Type = object : TypeToken<GenreAttributes?>() {}.type
        return gson.fromJson(genreAttributesString, type)
    }
}