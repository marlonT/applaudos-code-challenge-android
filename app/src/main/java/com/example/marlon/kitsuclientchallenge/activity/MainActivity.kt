package com.example.marlon.kitsuclientchallenge.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.marlon.kitsuclientchallenge.R
import com.example.marlon.kitsuclientchallenge.fragment.AnimeListFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // Creates a new fragment
        val animeListFragment = AnimeListFragment()
        supportFragmentManager.beginTransaction().replace(R.id.container_layout, animeListFragment)
            .commit()
    }
}