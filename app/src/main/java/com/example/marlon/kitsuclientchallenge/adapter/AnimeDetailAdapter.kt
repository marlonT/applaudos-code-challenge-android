package com.example.marlon.kitsuclientchallenge.adapter

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.example.marlon.kitsuclientchallenge.R
import com.example.marlon.kitsuclientchallenge.model.Anime
import com.example.marlon.kitsuclientchallenge.utils.GlideApp
import kotlinx.android.synthetic.main.anime_detail_item.view.*

class AnimeDetailAdapter(
    context: Context,
    private val selectedOption: SelectedOption
) : RecyclerView.Adapter<AnimeDetailAdapter.AnimeDetailViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var animes: ArrayList<Anime?> = ArrayList()

    inner class AnimeDetailViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    interface SelectedOption {
        fun openYouTube(id: String)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AnimeDetailViewHolder {
        val itemView = inflater.inflate(R.layout.anime_detail_item, parent, false)
        return AnimeDetailViewHolder(itemView)
    }


    // Show the anime's detail
    override fun onBindViewHolder(holder: AnimeDetailViewHolder, position: Int) {
        val current = animes[position]
        val view = holder.itemView
        current?.attributes?.apply {
            view.canonicalName.text = canonicalTitle
            view.animeName.text = mainTitle
            val typeAndEpisodeCount =
                "$showType, $episodeCount"
            view.type.text = typeAndEpisodeCount
            val year = "$startDate till $endDate"
            view.year.text = year
            val genres = genresToString()
            view.genres.text = genres
            view.averageRating.text = averageRating
            view.ageRating.text = ageRating
            view.episodeDuration.text = episodeLength
            view.airingStatus.text = airingStatus
            view.synopsis.text = synopsis
            view.youTube.text = youtubeVideoId
            view.youTube.setOnClickListener { selectedOption.openYouTube(youtubeVideoId!!) }
        }
        val imgUrl = current?.attributes?.posterImage?.original
        val thumbnail = view.animeThumbnail
        val progress = view.progress

        // Show the anime's image
        GlideApp.with(holder.itemView)
            .load(imgUrl)
            .override(350, 500)
            .centerCrop()
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    progress.visibility = View.GONE
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    progress.visibility = View.GONE
                    return false
                }
            })
            .into(thumbnail)
    }

    internal fun setAnimes(animes: ArrayList<Anime?>) {
        this.animes = animes
        notifyDataSetChanged()
    }

    override fun getItemCount() = animes.size
}