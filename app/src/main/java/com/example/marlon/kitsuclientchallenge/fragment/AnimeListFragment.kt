package com.example.marlon.kitsuclientchallenge.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.marlon.kitsuclientchallenge.R
import com.example.marlon.kitsuclientchallenge.activity.AnimeDetailActivity
import com.example.marlon.kitsuclientchallenge.adapter.AnimeListAdapter
import com.example.marlon.kitsuclientchallenge.adapter.GenresListAdapter
import com.example.marlon.kitsuclientchallenge.model.Genre
import com.example.marlon.kitsuclientchallenge.model.KitsuDatabase
import com.example.marlon.kitsuclientchallenge.repository.GenreRepository
import kotlinx.android.synthetic.main.fragment_anime_list.view.*

// Show the lis of genres in a RecyclerView
class AnimeListFragment : Fragment(), GenreRepository.GenreResponse, AnimeListAdapter.SelectedItem {

    private lateinit var adapter: GenresListAdapter
    private var database: KitsuDatabase? = null
    private var genreRepository: GenreRepository? = null

    override fun onGenresResult(genres: ArrayList<Genre?>) {
        adapter.setGenres(genres)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        database = KitsuDatabase.getDatabase(context!!)
        genreRepository = GenreRepository(database?.genreDao(), this)
        genreRepository?.getGenres()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_anime_list, container, false)
        val recyclerView = view.genresRecycler
        adapter = GenresListAdapter(context!!, this)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(context)
        return view
    }

    // Change to detail view
    override fun onItemSelected(position: Int, genre: String) {
        val bundle = Bundle()
        bundle.putString(AnimeDetailFragment.GENRE, genre)
        bundle.putInt(AnimeDetailFragment.POSITION, position)
        val intent = Intent(context, AnimeDetailActivity::class.java)
        intent.putExtras(bundle)
        activity?.startActivity(intent)
    }
}