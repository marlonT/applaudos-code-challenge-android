package com.example.marlon.kitsuclientchallenge.model

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.marlon.kitsuclientchallenge.dao.AnimeDao
import com.example.marlon.kitsuclientchallenge.dao.GenreDao
import com.example.marlon.kitsuclientchallenge.model.typeconverter.AnimeAttributesConverter
import com.example.marlon.kitsuclientchallenge.model.typeconverter.GenreAttributesConverter

@Database(
    entities = [
        Anime::class,
        Genre::class
    ], version = 1, exportSchema = false
)
@TypeConverters(GenreAttributesConverter::class, AnimeAttributesConverter::class)

abstract class KitsuDatabase : RoomDatabase() {
    abstract fun animeDao(): AnimeDao
    abstract fun genreDao(): GenreDao

    companion object {
        @Volatile
        private var instance: KitsuDatabase? = null

        fun getDatabase(
            context: Context
        ): KitsuDatabase? {
            // if the INSTANCE is not null, then return it,
            // if it is, then create the database
            return instance ?: synchronized(this) {
                instance = Room.databaseBuilder(
                    context.applicationContext,
                    KitsuDatabase::class.java,
                    "kitsu_database"
                ).build()
                // return instance
                instance
            }
        }
    }
}