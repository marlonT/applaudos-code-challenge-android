package com.example.marlon.kitsuclientchallenge.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class GenreAttributes(
    @SerializedName("name")
    val name: String?,
    @SerializedName("slug")
    val slug: String?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(slug)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<GenreAttributes> {
        override fun createFromParcel(parcel: Parcel): GenreAttributes {
            return GenreAttributes(parcel)
        }

        override fun newArray(size: Int): Array<GenreAttributes?> {
            return arrayOfNulls(size)
        }
    }
}
