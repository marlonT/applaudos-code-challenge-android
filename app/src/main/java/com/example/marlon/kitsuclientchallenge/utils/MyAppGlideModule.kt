package com.example.marlon.kitsuclientchallenge.utils

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

// Class for use AppGlide
@GlideModule
class MyAppGlideModule : AppGlideModule()